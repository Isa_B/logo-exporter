package demo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

public class RestDemo {
  static String username = null;
  static String password = null;
  
  public static void main(String[] args) throws Exception {
    // start a session

    JSONArray result = (JSONArray)doJSONCall("SessionService", "login", new String[] { "michael.herget", null, "donau2003", null });

    System.out.println(result);
        
    username = "michael.herget";
    password = result.get(1).toString();
    
    String query = "<query><select>page.id</select></query>";
    
    System.out.println(doJSONCall("PageService", "search", new Object[] { query, false }));

    query = "<query><select>page.id</select><select>page.keyword</select><where><field name=\"page.keyword\" comparator=\"like\" value=\"%POL%\"/></where><orderby index=\"0\">page.keyword</orderby><limit>500</limit></query>";

    System.out.println(doJSONCall("PageService", "search", new Object[] { query, false }));
  }


  
  // HILFSMETHODEN //
  
  public static Object doJSONCall(String service, String method, Object[] args) throws Exception {
    StringBuilder urlStr = new StringBuilder();
    urlStr.append("http://redweb3.donaukurier.de:8080/server/dv/rest/8100/").append(service).append('/').append(method);
    
    StringBuilder argsStr = new StringBuilder();
    if (args!=null && args.length!=0) {
      int i=0;
      for (Object arg : args) {
        if (arg!=null) {
          if (i>0)
            argsStr.append('&');
          argsStr.append("arg").append(i).append('=').append(URLEncoder.encode(JSONValue.toJSONString(arg), "UTF-8"));
        }
        i++;
      }
    }
    
    if (username!=null && password!=null) {
      if (argsStr.length()==0)
        argsStr.append('?');
      else
        argsStr.append('&');
      argsStr.append("dv_user=").append(URLEncoder.encode(username, "UTF-8"));
      argsStr.append("&dv_password=").append(URLEncoder.encode(password, "UTF-8"));
    }
    
    if (argsStr.length()>0)
      urlStr.append('?').append(argsStr);
    
    return new JSONParser().parse(fetchURL(urlStr.toString()));
  }
  
  public static String fetchURL(String url) throws IOException {
    System.out.println("Doing call on URL "+url);
    
    HttpClient client = new HttpClient();
    HttpMethod method = new GetMethod(url);

    int statusCode = client.executeMethod(method);

    if (statusCode != HttpStatus.SC_OK)
      throw new IOException("HTTP request failed");

    Header contentTypeHeader = method.getResponseHeader("Content-Type");
    if (contentTypeHeader==null)
        throw new IOException("unknown content-type");
    
    String contentType = contentTypeHeader.getValue().trim().toLowerCase();
    
    InputStream in = method.getResponseBodyAsStream();
    
    byte[] buffer = new byte[16384];        
    ByteArrayOutputStream fout = new ByteArrayOutputStream();
    int l;
    while ((l=in.read(buffer))>0) {
        fout.write(buffer,0,l);
    }
    fout.close();
    in.close();

    if (!"application/json".equals(contentType))
      throw new IOException("server returned an error: "+new String(fout.toByteArray(), "UTF-8"));

    // since JSON uses it's own way of encoding special characters,
    // it is totally acceptable to use UTF-8
    return new String(fout.toByteArray(), "UTF-8");
  }
}
