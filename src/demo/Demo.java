package demo;

import java.net.URL;
import java.io.File;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import eu.red_web.server.client.ClientFactoryX;
import eu.red_web.server.client.servlet.ServletClientFactory;

public class Demo {
  public static void main(String[] args) throws Exception {
    String serverURL = "http://sunrise.red-web.lan:8080/server/dv?dvPort=8100";
    ClientFactoryX factory = ClientFactoryX.connect(serverURL, "bohni", "bohni", "Demoprojekt");
    ServletClientFactory sf = new ServletClientFactory(new URL(serverURL.substring(0, serverURL.lastIndexOf('/') /* typical but may fail */)), factory.getUser(), null, factory.getSessionKey());
    try {

      /* Ab hier Code schreiben, der die Services des Print Publishers verwendet */
      
      // Name der Kategorie wird eingelesen, die in ein Verzeichnis exportiert werden soll
      InputStreamReader inputStreamCategory = new InputStreamReader(System.in);
      BufferedReader bufferCategory = new BufferedReader(inputStreamCategory);
      System.out.print("Name der Logo-Kategorie, die Sie exportieren m�chten: ");
      String categoryName = bufferCategory.readLine();
      
      // Erstellen eines neuen Verzeichnisses
      InputStreamReader inputStreamPath = new InputStreamReader(System.in);
      BufferedReader bufferPath = new BufferedReader(inputStreamPath);
      System.out.print("Geben Sie den Dateipfad an, unter dem ein Ordner mit den exportierten Logos erstellt werden soll: ");
      String path = bufferPath.readLine();
      
      // Ersetzt alle Backslashs im Pfad mit Slashs
      String newPath = path.replace("\\", "/");
      path = newPath;
      
      // Sorgt daf�r, dass Pfad mit Slash endet
      if(!path.endsWith("/")){
        newPath = path + "/";
        path = newPath;
      }
      
      // Wirft Exception wenn Dateipfad nicht existiert
      if (!new File(path).exists())
      {
         throw new Exception("Dateipfad existiert nicht.");
      }
      
      File NewDirectory = new File(path + categoryName);
      NewDirectory.mkdir();
    	
    		String query = 	
    				"<query>"
    					+ "<select>logo.id</select>"
    				  + "<select>logo.keyword</select>"
    					+ "<select>logo.mimetype</select>"
    					+ 	"<where>"
    					+		  "<field name=\"logo.category.category\" comparator=\"equal\" value=\"" + categoryName + "\" />"
    					+ 	"</where>"
    				+ "</query>";
    
    		String searchResult = factory.getLogoDVProxy().search(query, false);
    		
    		while(searchResult.contains("value=")){
    		  String logoID = searchResult.substring(searchResult.indexOf("value=") + 7, searchResult.indexOf("\"/>"));
    		  String newSearchResult = searchResult.substring(searchResult.indexOf("\"/>") + 3);
    		  searchResult = newSearchResult;
        
    		  String fileMIME = newSearchResult.substring(newSearchResult.indexOf("value=") + 7, newSearchResult.indexOf("\"/>"));
          newSearchResult = searchResult.substring(searchResult.indexOf("\"/>") + 3);
          searchResult = newSearchResult; 
    		  
    		  String fileName = newSearchResult.substring(newSearchResult.indexOf("value=") + 7, newSearchResult.indexOf("\"/>"));
    		  newSearchResult = searchResult.substring(searchResult.indexOf("\"/>") + 3);
    		  searchResult = newSearchResult;
    		  
          String fileExtension = null;
          
          // wandelt die g�ngigen Bild-MIMEtypes in FileExtensions um
          if(fileMIME.contains("application/pdf") || fileMIME.equals("image/jpeg") || fileMIME.equals("image/png") || fileMIME.equals("image/tiff")){
            if(fileMIME.equals("application/postscript")) {fileExtension = ".ai";}
            if(fileMIME.equals("application/pdf")) {fileExtension = ".pdf";}
            if(fileMIME.equals("image/jpeg")) {fileExtension = ".jpeg";}
            if(fileMIME.equals("image/png")) {fileExtension = ".png";}
            if(fileMIME.equals("image/tiff")) {fileExtension = ".tiff";}
            
            // Entferne FileExtensions aus Namen
            if(fileName.contains(".")){
              String newFileName = fileName.substring(0, fileName.lastIndexOf("."));
              fileName = newFileName;
            } // end-if
          } // end-if
          
          else{
            // versucht, bei Nichterkennung des MIMEtypes, Extension aus Dateinamen zu �bernehmen
            if(fileName.contains(".")){
              fileExtension = fileName.substring(fileName.lastIndexOf("."));
              String newFileName = fileName.substring(0, fileName.lastIndexOf("."));
              fileName = newFileName;
            } // end-if
            else{
              // enth�lt keine Extension, falls sie nicht erkannt werden kann
              fileExtension = "";
            } // end-else
    		  } // end-else
          
    		  sf.getLogoClient().downloadCurrentHighres(logoID, new File(path + categoryName + "/" + fileName + "-" + logoID + fileExtension), null);
    		} // end-while
    		System.out.println("Die Logos wurden in den Ordner " + path + categoryName + "/ exportiert.");
    		
    } finally {
      factory.disconnect();
    }
  }
} 		
